#!/usr/bin/env python
import unittest

from elephant import Elephant

class ElephantTestCase(unittest.TestCase):
    def test_elephants_are_jumbo(self):
        elephant = Elephant()
        self.assertTrue(elephant.is_jumbo)

    def test_elephants_can_furl(self):
        elephant = Elephant()
        self.assertTrue(elephant.can_furl)

if __name__ == '__main__':
    unittest.main()

