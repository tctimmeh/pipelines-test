class Elephant(object):
    @property
    def is_jumbo(self):
        return True

    @property
    def can_furl(self):
        try:
            import furl
            return True
        except:
            return False
